package normalize

import (
	"context"
	"strings"

	"github.com/rs/zerolog"
	"gitlab.com/mgsearch/sizrr"
	"gitlab.com/mgsearch/sizrr-server/grpc/v1/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// NewNormalizeHandler is just a constructor for NormalizeHandler
func NewNormalizeHandler(logger *zerolog.Logger) *NormalizeHandler {
	return &NormalizeHandler{
		normalizer: sizrr.New(),
		logger:     logger,
	}
}

// NormalizeHandler provides simple normalization handler.
type NormalizeHandler struct {
	proto.UnimplementedNormalizerServer
	normalizer *sizrr.Sizrr
	logger     *zerolog.Logger
}

// Normalize processes fashion sizes normalization using external normalization library and implementing gRPC interface.
func (handler *NormalizeHandler) Normalize(ctx context.Context, input *proto.Request) (*proto.Response, error) {
	handler.logger.Debug().Str("protocol", "gRPC").Str("event", "request").Str("service", "Normalizer").Str("request_data", input.String()).Send()

	// validate text
	if strings.TrimSpace(input.Text) == "" {
		err := status.Error(codes.InvalidArgument, "text must not be empty")
		handler.logger.Debug().Str("protocol", "gRPC").Str("event", "request").Str("service", "Normalizer").Err(err).Msg("validation error")
		return nil, err
	}

	// and finally generate normalized sizes
	sizes := handler.normalizer.Normalize(input.Text)
	output := make([]*proto.Response_Sizes, 0, len(sizes))
	for _, size := range sizes {
		output = append(output, &proto.Response_Sizes{
			Size: size.Size,
			Type: size.Type,
		})
	}
	response := proto.Response{Sizes: output}
	handler.logger.Debug().Str("protocol", "gRPC").Str("event", "response").Str("service", "Normalizer").Str("response_data", response.String()).Send()

	return &response, nil
}
