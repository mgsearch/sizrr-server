package tests

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mgsearch/sizrr-server/grpc/v1/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
)

const normalizeEndpoint = "http://sizrrapi:11111/v1/normalize"

// TestHTTP tests real HTTP calls to sizrr API
func TestHTTP(t *testing.T) {
	t.Run("test classical response", func(t *testing.T) {
		response, err := http.Post(normalizeEndpoint, "application/json", bytes.NewReader([]byte(`{"text":"EUR 43 2/3"}`)))
		if err != nil {
			t.Error(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, `{"sizes":[{"size":"44","type":"numeric"}]}`, string(body))
	})

	t.Run("test empty text error", func(t *testing.T) {
		response, err := http.Post(normalizeEndpoint, "application/json", bytes.NewReader([]byte(
			`{"text":" "}`,
		)))
		if err != nil {
			t.Error(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, `{"error":"text must not be empty"}`, string(body))
	})

	t.Run("test unknown endpoint error", func(t *testing.T) {
		response, err := http.Post(normalizeEndpoint+"whatever", "application/json", bytes.NewReader(nil))
		if err != nil {
			t.Error(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, `{"error":"endpoint not found"}`, string(body))
	})

	t.Run("test swagger", func(t *testing.T) {
		response, err := http.Get("http://sizrrapi:11111/v1/doc/index.html")
		if err != nil {
			t.Error(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, 200, response.StatusCode)
		assert.Contains(t, string(body), "Fashion Sizes Normalization - Swagger doc") // just check for title
	})
}

// TestGRPC tests real gRPC calls to sizrr API
func TestGRPC(t *testing.T) {
	connection, err := grpc.Dial("sizrrapi:22222", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatal(err)
	}
	defer connection.Close()
	grpcClient := proto.NewNormalizerClient(connection)

	// test classical response
	t.Run("test classical response", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		response, err := grpcClient.Normalize(ctx, &proto.Request{
			Text: "EUR 43 2/3",
		})
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, []*proto.Response_Sizes{
			{
				Size: "44",
				Type: "numeric",
			},
		}, response.Sizes)
	})

	t.Run("test empty text error", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		response, err := grpcClient.Normalize(ctx, &proto.Request{
			Text: "  ",
		})
		statusError, ok := status.FromError(err)
		if !ok {
			t.Error(err)
		}
		assert.Equal(t, codes.InvalidArgument, statusError.Code())
		assert.Equal(t, "text must not be empty", statusError.Message())
		assert.Nil(t, response)
	})
}
