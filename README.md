# Fashion Sizes Normalizer Server (HTTP/2 and gRPC)

#### by MALL Group Search Lab

Sizrr Server is a stateless server providing **REST API** and **gRPC API** for **clothing & shoes sizes normalization** using [https://gitlab.com/mgsearch/sizrr](https://gitlab.com/mgsearch/sizrr) - MALL Group Search Lab library clothes & shoes sizes normalizer.

It tries to understand **input size(s)** (or basically input text) and **converts it into normalized sizes**, e.g. `"XXXL regular / EUR 47"` converts into
```json
[
  {
    "size": "3XL",
    "type": "SML"
  },
  {
    "size": "47",
    "type": "numeric"
  }
]
```

## Usage

#### HTTP/2

We provide **REST API at** [https://sizenormalizer.api.mallsearchlab.cz](https://sizenormalizer.api.mallsearchlab.cz)

**Request example**: `POST https://sizenormalizer.api.mallsearchlab.cz/v1/normalize` with body
```json
{
  "text": "XXXL regular / EUR 47"
}
```

**Response**: 
```json
[
  {
    "size": "3XL",
    "type": "SML"
  },
  {
    "size": "47",
    "type": "numeric"
  }
]
```

No credentials or security tokens needed.

#### gRPC

We provide **gRPC API at** [sizenormalizer.grpc.mallsearchlab.cz](sizenormalizer.grpc.mallsearchlab.cz) port 80

**Protocol contract**:

```protobuf
syntax = "proto3";

package proto.v1;

service Normalizer {
  rpc Normalize (Request) returns (Response) {}
}

message Request {
  string text = 1;
}

message Response {
  message Sizes {
    string size = 1;
    string type = 2;
  }

  repeated Sizes sizes = 1;
}
```

No credentials or security tokens needed.

## Documentation

#### HTTP/2

See our [Swagger API documentation](https://sizenormalizer.api.mallsearchlab.cz/v1/doc/index.html)

#### gRPC

See our [proto file](../grpc/v1/proto/normalize.proto)

Working example of your own gRPC client in Golang:
```go
package main

import (
	"context"
	"log"
	"time"

	"gitlab.com/mgsearch/sizrr-server/grpc/v1/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	// this is working example you can test right now

	// establish connection
	connection, err := grpc.Dial(
		"sizenormalizer.grpc.mallsearchlab.cz:80",
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		panic(err)
	}
	defer connection.Close()

	// create gRPC client
	// this proto.NewNormalizerClient is generated from proto file by protobuf
	// see https://grpc.io/docs/languages/go/quickstart/
	// or go to our folder gitlab.com/mgsearch/sizrr-server/grpc/v1/proto
	// and run `protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative normalize.proto`
	grpcClient := proto.NewNormalizerClient(connection)

	// and finally make your gRPC call
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	response, err := grpcClient.Normalize(ctx, &proto.Request{
		Text:         "XXXL regular / EUR 47",
	})
	if err != nil {
		panic(err)
	}

	// response:
	// sizes:{size:"3XL"  type:"SML"}  sizes:{size:"47"  type:"numeric"}
	log.Println(response)
}
```

## Benchmarks - HTTP

For benchmarks [Bombardier](https://pkg.go.dev/github.com/codesenberg/bombardier) was used, test duration 10 sec., 125 concurrent connections.

1 pod, no CPU and memory limits, local network

| Statistics | Avg | Stdev | Max
| --- |---------|---------| ---
| Reqs. / sec | 76402.24 | 4701.88 | 87086.42
| Latency | 1.63 ms | 489.72 us | 21.09 ms
  
HTTP codes: 1xx - 0, 2xx - 763895, 3xx - 0, 4xx - 0, 5xx - 0, others - 0, throughput: 23.02 MB/s

## Deployment 

#### Docker Image

If you want to run your local server e.g. as microservice (as it will be much, much faster than calling it to the other side of this planet via internet), you can simply use our docker images from our [docker registry](https://gitlab.com/mgsearch/sizrr-server/container_registry/3076788) or just use our source:

`registry.gitlab.com/mgsearch/sizrr-server:1.0.1`

#### Kubernetes

If you run Kubernetes cluster, you can deploy your own server with a snap of your finger. See our [k8s konfigs](../docker/k8s) and simply apply them (some minor tweaking should be necessary depending on your cluster configuration).
