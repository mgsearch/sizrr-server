package normalize

import (
	"encoding/json"
	"strings"

	"gitlab.com/mgsearch/sizrr"

	"github.com/valyala/fasthttp"
)

// NewNormalizeHandler is just a constructor for NormalizeHandler
func NewNormalizeHandler() *NormalizeHandler {
	return &NormalizeHandler{
		normalizer: sizrr.New(),
	}
}

// NormalizeHandler provides simple handler for handling fashion sizes normalization.
type NormalizeHandler struct {
	normalizer *sizrr.Sizrr
}

type request struct {
	Text string `json:"text"`
}

// Normalize is fashion sizes normalization handler for HTTP,
func (handler *NormalizeHandler) Normalize(ctx *fasthttp.RequestCtx) {
	// unmarshal request body
	body := request{}
	err := json.Unmarshal(ctx.PostBody(), &body)
	if err != nil {
		handler.set401Response(ctx, "please provide text in correct request JSON")
		return
	}

	// validate text
	if strings.TrimSpace(body.Text) == "" {
		handler.set401Response(ctx, "text must not be empty")
		return
	}

	// and finally generate normalized sizes
	sizesJson, err := json.Marshal(handler.normalizer.Normalize(body.Text))
	if err != nil {
		handler.set500Response(ctx)
		return
	}

	ctx.SetBody([]byte(`{"sizes":` + string(sizesJson) + `}`))
}

func (handler *NormalizeHandler) set401Response(ctx *fasthttp.RequestCtx, message string) {
	ctx.SetStatusCode(fasthttp.StatusBadRequest)
	ctx.SetBody([]byte(`{"error":"` + message + `"}`))
}

func (handler *NormalizeHandler) set500Response(ctx *fasthttp.RequestCtx) {
	ctx.SetStatusCode(fasthttp.StatusInternalServerError)
	ctx.SetBody([]byte(`{"error":"internal server error"}`))
}
