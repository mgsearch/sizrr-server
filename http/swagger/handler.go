package swagger

import (
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/valyala/fasthttp"
)

// NewSwaggerHandler is just a constructor for SwaggerHandler
func NewSwaggerHandler() *SwaggerHandler {
	return &SwaggerHandler{}
}

// SwaggerHandler provides simple handler for handling Swager UI.
type SwaggerHandler struct{}

// Swag is handler for Swagger API documentation.
func (handler *SwaggerHandler) Swag(ctx *fasthttp.RequestCtx, swaggerFilesFullPath string) {
	fileName := strings.Replace(string(ctx.Path()), "/v1/doc/", "", 1)
	if fileName == "" {
		ctx.Redirect("/v1/doc/index.html", http.StatusMovedPermanently)
		return
	}

	data, err := ioutil.ReadFile(swaggerFilesFullPath + "/" + fileName) // if swagger does not work locally, add path to your ENV SWAGGER_FILES_FULL_PATH
	if err != nil {
		ctx.Response.SetStatusCode(http.StatusNotFound)
		return
	}

	contentType := "text/html; charset=utf-8"
	if strings.HasSuffix(fileName, ".css") {
		contentType = "text/css"
	} else if strings.HasSuffix(fileName, ".js") {
		contentType = "application/javascript"
	} else if strings.HasSuffix(fileName, ".png") {
		contentType = "image/png"
	} else if strings.HasSuffix(fileName, ".json") {
		contentType = "application/json"
	}

	ctx.Response.Header.Set("Content-Type", contentType)
	ctx.SetBody(data)
}
